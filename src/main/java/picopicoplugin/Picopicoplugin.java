package picopicoplugin;

import java.util.Arrays;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.SkullType;
import org.bukkit.Sound;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.SkullMeta;
import org.bukkit.plugin.RegisteredServiceProvider;
import org.bukkit.plugin.java.JavaPlugin;

import net.milkbowl.vault.economy.Economy;

public class Picopicoplugin extends JavaPlugin implements Listener {

	public static int headPrice = 10;
	public static int headdeposit = 5;
	public static ItemStack head_delivery = new ItemStack(Material.SKULL_ITEM);
	public static SkullMeta clickMeta;
	public static ItemMeta itemMeta_reset= head_delivery.getItemMeta();

	private static Economy economy = null;

	private boolean setupEconomy() {
		if (getServer().getPluginManager().getPlugin("Vault") == null) {
			return false;
		}
		RegisteredServiceProvider<Economy> rsp = getServer().getServicesManager().getRegistration(Economy.class);
		if (rsp == null) {
			return false;
		}
		economy = rsp.getProvider();
		return economy != null;
	}

	//起動
    @Override
    public void onEnable() {

		//Vault未導入の場合はエラー
		if (!setupEconomy() ) {
			getLogger().severe(String.format("[%s] - Disabled due to no Vault dependency found!", getDescription().getName()));
			getServer().getPluginManager().disablePlugin(this);
			return;
		}

        getLogger().info("正常に起動");

        FileConfiguration config = this.getConfig();
        config.options().copyDefaults(true);
        this.saveConfig();
        this.getServer().getPluginManager().registerEvents(this, this);
    }

    //終了
    @Override
    public void onDisable() {
        getLogger().info("終了");
    }

    //コマンド
    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
    	if (command.getName().equalsIgnoreCase("headbuy")) {

			if (!(sender instanceof Player)) {//コンソールから実行した場合
				sender.sendMessage(ChatColor.AQUA + "サーバー側からはHeadBuyを実行できません。");
				return true;
			}

			Player p = (Player)sender;

			Inventory headinv = Bukkit.createInventory(null, 45, "頭の購入");

			int countOnlineUsers = 0;
			for(Player all:getServer().getOnlinePlayers()){
				//頭の生成
				ItemStack headItem = new ItemStack(Material.SKULL_ITEM, 1, (short) SkullType.PLAYER.ordinal());
				//頭のメタ定義
				SkullMeta itemMeta = (SkullMeta) headItem.getItemMeta();
				itemMeta.setOwner(all.getName());
				//生成した頭部にメタを適用
				headItem.setItemMeta(itemMeta);
				headinv.setItem(countOnlineUsers, headItem);
				countOnlineUsers++;
			}


			p.openInventory(headinv);
			p.closeInventory();
			p.openInventory(headinv);

			return true;

		}
		return false;
	}

	@EventHandler
	public void onPlayerChoicingPrice(InventoryClickEvent event) {
		Player player = (Player) event.getWhoClicked();
		ItemStack clicked = event.getCurrentItem();
		Inventory inventory = event.getInventory();
		int slot = event.getSlot();

		if (inventory.getName().equals("金額の選択")){

				if(event.getRawSlot() <= 44) {

				if (slot == 0) {

					headPrice = 10;
					headdeposit = 9;


					ItemMeta itemMeta_better = head_delivery.getItemMeta();
					List<String> lore_better = Arrays.asList(ChatColor.RESET + clickMeta.getOwner() +"に10ドルを支払いました (" + player.getName() + ")");
					itemMeta_better.setLore(lore_better);
					itemMeta_better.setDisplayName(ChatColor.LIGHT_PURPLE + "[イイね]" + ChatColor.YELLOW +  clickMeta.getOwner() + "の頭");
					itemMeta_better.addItemFlags(ItemFlag.HIDE_ENCHANTS);
					head_delivery.setItemMeta(itemMeta_better);
					head_delivery.addUnsafeEnchantment(Enchantment.VANISHING_CURSE, 1);


					if(economy.has(player,headPrice)){

						event.setCancelled(true);
						player.closeInventory();

						//支払い
						economy.withdrawPlayer(player, headPrice);
						player.sendMessage(ChatColor.AQUA + "[HeadBuy] " + clickMeta.getOwner() + "さんのHeadを購入しました、ご利用ありがとう！");
						//受け取り
						Player depositor = Bukkit.getPlayer(clickMeta.getOwner());
						economy.depositPlayer(depositor, headdeposit);
						depositor.sendMessage(ChatColor.AQUA + "[HeadBuy] " + player.getDisplayName() + "さんがあなたのHeadを購入しました" + ChatColor.LIGHT_PURPLE + "[いいね]");

						player.getWorld().playSound(player.getLocation(),Sound.BLOCK_ANVIL_PLACE,1,3);
						depositor.getWorld().playSound(depositor.getLocation(),Sound.BLOCK_ANVIL_PLACE,1,3);

						for(Player op:getServer().getOnlinePlayers()){
							if(op.hasPermission("headbuy.notice")) {
								   op.sendMessage(ChatColor.AQUA + "[HeadBuy(op)] " + player.getDisplayName() + "さんが" + clickMeta.getOwner() + "さんのHeadを購入しました" + ChatColor.LIGHT_PURPLE + "[いいね]");
								}
						}

						getLogger().info("[HeadBuy] " + player.getDisplayName() + "さんが" + clickMeta.getOwner() + "さんのHeadを購入しました[いいね]");

						player.getInventory().addItem(head_delivery);

						head_delivery.setItemMeta(itemMeta_reset);

						}else{
							player.sendMessage(ChatColor.AQUA + "[HeadBuy] お代が足りません ($" + headPrice + ")");
							player.closeInventory();//GUIを閉じる

							head_delivery.setItemMeta(itemMeta_reset);
						}

				}

				if (slot == 2) {

					headPrice = 30;
					headdeposit = 27;

					ItemMeta itemMeta_good = head_delivery.getItemMeta();
					List<String> lore_good = Arrays.asList(ChatColor.RESET + clickMeta.getOwner() +"に30ドルを支払いました (" + player.getName() + ")");
					itemMeta_good.setLore(lore_good);
					itemMeta_good.setDisplayName(ChatColor.LIGHT_PURPLE + "[気に入った]" + ChatColor.YELLOW +  clickMeta.getOwner() + "の頭");
					itemMeta_good.addItemFlags(ItemFlag.HIDE_ENCHANTS);
					head_delivery.setItemMeta(itemMeta_good);
					head_delivery.addUnsafeEnchantment(Enchantment.VANISHING_CURSE, 1);

					if(economy.has(player,headPrice)){

						event.setCancelled(true);
						player.closeInventory();

						//支払い
						economy.withdrawPlayer(player, headPrice);
						player.sendMessage(ChatColor.AQUA + "[HeadBuy] " + clickMeta.getOwner() + "さんのHeadを購入しました、ご利用ありがとう！");
						//受け取り
						Player depositor = Bukkit.getPlayer(clickMeta.getOwner());
						economy.depositPlayer(depositor, headdeposit);
						depositor.sendMessage(ChatColor.AQUA + "[HeadBuy] " + player.getDisplayName() + "さんがあなたのHeadを購入しました" + ChatColor.LIGHT_PURPLE + "[気に入った]");

						player.getWorld().playSound(player.getLocation(),Sound.BLOCK_ANVIL_PLACE,1,3);
						depositor.getWorld().playSound(depositor.getLocation(),Sound.BLOCK_ANVIL_PLACE,1,3);

						for(Player op:getServer().getOnlinePlayers()){
							if(op.hasPermission("headbuy.notice")) {
								   op.sendMessage(ChatColor.AQUA + "[HeadBuy(op)] " + player.getDisplayName() + "さんが" + clickMeta.getOwner() + "さんのHeadを購入しました" + ChatColor.LIGHT_PURPLE + "[気に入った]");
								}
						}

						getLogger().info(player.getDisplayName() + "さんが" + clickMeta.getOwner() + "さんのHeadを購入しました[気に入った]");

						player.getInventory().addItem(head_delivery);

						head_delivery.setItemMeta(itemMeta_reset);

						}else{
							player.sendMessage(ChatColor.AQUA + "[HeadBuy] お代が足りません ($" + headPrice + ")");
							player.closeInventory();//GUIを閉じる

							head_delivery.setItemMeta(itemMeta_reset);
						}

				}

				if (slot == 4) {

					headPrice = 50;
					headdeposit = 45;

					ItemMeta itemMeta_like = head_delivery.getItemMeta();
					List<String> lore_like = Arrays.asList(ChatColor.RESET + clickMeta.getOwner() +"に50ドルを支払いました (" + player.getName() + ")");
					itemMeta_like.setLore(lore_like);
					itemMeta_like.setDisplayName(ChatColor.LIGHT_PURPLE + "[好き]" + ChatColor.YELLOW +  clickMeta.getOwner() + "の頭");
					itemMeta_like.addItemFlags(ItemFlag.HIDE_ENCHANTS);
					head_delivery.setItemMeta(itemMeta_like);
					head_delivery.addUnsafeEnchantment(Enchantment.VANISHING_CURSE, 1);
					head_delivery.addUnsafeEnchantment(Enchantment.DURABILITY, 1);
					head_delivery.addUnsafeEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, 1);


					if(economy.has(player,headPrice)){

						event.setCancelled(true);
						player.closeInventory();

						//支払い
						economy.withdrawPlayer(player, headPrice);
						player.sendMessage(ChatColor.AQUA + "[HeadBuy] " + clickMeta.getOwner() + "さんのHeadを購入しました、ご利用ありがとう！");
						//受け取り
						Player depositor = Bukkit.getPlayer(clickMeta.getOwner());
						economy.depositPlayer(depositor, headdeposit);
						depositor.sendMessage(ChatColor.AQUA + "[HeadBuy] " + player.getDisplayName() + "さんがあなたのHeadを購入しました" + ChatColor.LIGHT_PURPLE + "[好き]");

						for(Player op:getServer().getOnlinePlayers()){
							if(op.hasPermission("headbuy.notice")) {
								   op.sendMessage(ChatColor.AQUA + "[HeadBuy(op)] " + player.getDisplayName() + "さんが" + clickMeta.getOwner() + "さんのHeadを購入しました" + ChatColor.LIGHT_PURPLE + "[好き]");
								}
						}

						getLogger().info(player.getDisplayName() + "さんが" + clickMeta.getOwner() + "さんのHeadを購入しました[好き]");

						player.getWorld().playSound(player.getLocation(),Sound.BLOCK_ANVIL_PLACE,1,3);
						depositor.getWorld().playSound(depositor.getLocation(),Sound.BLOCK_ANVIL_PLACE,1,3);

						player.getInventory().addItem(head_delivery);

						head_delivery.setItemMeta(itemMeta_reset);

						}else{
							player.sendMessage(ChatColor.AQUA + "[HeadBuy] お代が足りません ($" + headPrice + ")");
							player.closeInventory();//GUIを閉じる

							head_delivery.setItemMeta(itemMeta_reset);
						}

				}

				if (slot == 6) {

					headPrice = 100;
					headdeposit = 90;

					ItemMeta itemMeta_awesome = head_delivery.getItemMeta();
					List<String> lore_awesome = Arrays.asList(ChatColor.RESET + clickMeta.getOwner() +"に100ドルを支払いました (" + player.getName() + ")");
					itemMeta_awesome.setLore(lore_awesome);
					itemMeta_awesome.setDisplayName(ChatColor.LIGHT_PURPLE + "[大好き]" + ChatColor.YELLOW +  clickMeta.getOwner() + "の頭");
					itemMeta_awesome.addItemFlags(ItemFlag.HIDE_ENCHANTS);
					head_delivery.setItemMeta(itemMeta_awesome);
					head_delivery.addUnsafeEnchantment(Enchantment.VANISHING_CURSE, 1);
					head_delivery.addUnsafeEnchantment(Enchantment.DURABILITY, 2);
					head_delivery.addUnsafeEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, 2);
					head_delivery.addUnsafeEnchantment(Enchantment.OXYGEN, 2);

					if(economy.has(player,headPrice)){

						event.setCancelled(true);
						player.closeInventory();

						//支払い
						economy.withdrawPlayer(player, headPrice);
						player.sendMessage(ChatColor.AQUA + "[HeadBuy] " + clickMeta.getOwner() + "さんのHeadを購入しました、ご利用ありがとう！");
						//受け取り
						Player depositor = Bukkit.getPlayer(clickMeta.getOwner());
						economy.depositPlayer(depositor, headdeposit);
						depositor.sendMessage(ChatColor.AQUA + "[HeadBuy] " + player.getDisplayName() + "さんがあなたのHeadを購入しました" + ChatColor.LIGHT_PURPLE + "[大好き]");

						player.getWorld().playSound(player.getLocation(),Sound.BLOCK_ANVIL_PLACE,1,3);
						depositor.getWorld().playSound(depositor.getLocation(),Sound.BLOCK_ANVIL_PLACE,1,3);

						for(Player op:getServer().getOnlinePlayers()){
							if(op.hasPermission("headbuy.notice")) {
								   op.sendMessage(ChatColor.AQUA + "[HeadBuy(op)] " + player.getDisplayName() + "さんが" + clickMeta.getOwner() + "さんのHeadを購入しました" + ChatColor.LIGHT_PURPLE + "[大好き]");
								}
						}

						getLogger().info(player.getDisplayName() + "さんが" + clickMeta.getOwner() + "さんのHeadを購入しました[大好き]");

						player.getInventory().addItem(head_delivery);

						head_delivery.setItemMeta(itemMeta_reset);

						}else{
							player.sendMessage(ChatColor.AQUA + "[HeadBuy] お代が足りません ($" + headPrice + ")");
							player.closeInventory();//GUIを閉じる

							head_delivery.setItemMeta(itemMeta_reset);
						}

				}

				if (slot == 8) {

					headPrice = 200;
					headdeposit = 180;

					ItemMeta itemMeta_love = head_delivery.getItemMeta();
					List<String> lore_love = Arrays.asList(ChatColor.RESET + clickMeta.getOwner() +"に200ドルを支払いました (" + player.getName() + ")");
					itemMeta_love.setLore(lore_love);
					itemMeta_love.setDisplayName(ChatColor.LIGHT_PURPLE + "[マジラブ]" + ChatColor.YELLOW +  clickMeta.getOwner() + "の頭");
					itemMeta_love.addItemFlags(ItemFlag.HIDE_ENCHANTS);
					head_delivery.setItemMeta(itemMeta_love);
					head_delivery.addUnsafeEnchantment(Enchantment.VANISHING_CURSE, 1);
					head_delivery.addUnsafeEnchantment(Enchantment.BINDING_CURSE, 1);
					head_delivery.addUnsafeEnchantment(Enchantment.DURABILITY, 3);
					head_delivery.addUnsafeEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, 3);
					head_delivery.addUnsafeEnchantment(Enchantment.OXYGEN, 3);
					head_delivery.addUnsafeEnchantment(Enchantment.WATER_WORKER, 3);


					if(economy.has(player,headPrice)){

						event.setCancelled(true);
						player.closeInventory();

						//支払い
						economy.withdrawPlayer(player, headPrice);
						player.sendMessage(ChatColor.AQUA + "[HeadBuy] " + clickMeta.getOwner() + "さんのHeadを購入しました、ご利用ありがとう！");
						//受け取り
						Player depositor = Bukkit.getPlayer(clickMeta.getOwner());
						economy.depositPlayer(depositor, headdeposit);
						depositor.sendMessage(ChatColor.AQUA + "[HeadBuy] " + player.getDisplayName() + "さんがあなたのHeadを購入しました" + ChatColor.LIGHT_PURPLE + "[マジラブ]");

						for(Player op:getServer().getOnlinePlayers()){
							if(op.hasPermission("headbuy.notice")) {
								   op.sendMessage(ChatColor.AQUA + "[HeadBuy(op)] " + player.getDisplayName() + "さんが" + clickMeta.getOwner() + "さんのHeadを購入しました" + ChatColor.LIGHT_PURPLE + "[マジラブ]");
								}
						}

						getLogger().info(player.getDisplayName() + "さんが" + clickMeta.getOwner() + "さんのHeadを購入しました[マジラブ]");

						player.getWorld().playSound(player.getLocation(),Sound.BLOCK_ANVIL_PLACE,1,3);
						depositor.getWorld().playSound(depositor.getLocation(),Sound.BLOCK_ANVIL_PLACE,1,3);

						player.getInventory().addItem(head_delivery);

						head_delivery.setItemMeta(itemMeta_reset);

						}else{
							player.sendMessage(ChatColor.AQUA + "[HeadBuy] お代が足りません ($" + headPrice + ")");
							player.closeInventory();//GUIを閉じる

							head_delivery.setItemMeta(itemMeta_reset);
						}

				}

			}
		}
	}


	@EventHandler
	public void onPlayerBuyingHead(InventoryClickEvent event) {

		Inventory headinv_choice = Bukkit.createInventory(null, 9, "金額の選択");
		Player player = (Player) event.getWhoClicked();
		ItemStack clicked = event.getCurrentItem();
		Inventory inventory = event.getInventory();


		//インベントリはheadbuyのものか？
		if (inventory.getName().equals("頭の購入")) {
			//クリックしたものは頭か？
			if(clicked.getType() == Material.SKULL_ITEM){
				//クリックしたインベントリの検知 44以下で上インベントリ
				if(event.getRawSlot() <= 44) {

		clickMeta = (SkullMeta) clicked.getItemMeta();

		getLogger().info(clickMeta.getOwner());
		event.setCancelled(true);
		player.closeInventory();

		head_delivery = clicked;


		//ここめっちゃ汚いけど許して＞＜
	ItemStack choicegui_better = new ItemStack(Material.STONE);//GUI用アイコン作成
	ItemStack choicegui_good = new ItemStack(Material.COAL);
	ItemStack choicegui_like = new ItemStack(Material.IRON_INGOT);
	ItemStack choicegui_awesome = new ItemStack(Material.GOLD_INGOT);
	ItemStack choicegui_love = new ItemStack(Material.DIAMOND);

	ItemMeta itemMeta_better = choicegui_better.getItemMeta();//アイコンのメタ作成
	ItemMeta itemMeta_good = choicegui_good.getItemMeta();
	ItemMeta itemMeta_like = choicegui_like.getItemMeta();
	ItemMeta itemMeta_awesome = choicegui_awesome.getItemMeta();
	ItemMeta itemMeta_love = choicegui_love.getItemMeta();

	itemMeta_better.setDisplayName(ChatColor.YELLOW + "イイね(better):10$");//アイコンの表示する名前の指定
	itemMeta_good.setDisplayName(ChatColor.YELLOW + "気に入った(good):30$");
	itemMeta_like.setDisplayName(ChatColor.YELLOW + "好き(like):50$");
	itemMeta_awesome.setDisplayName(ChatColor.YELLOW + "大好き(awesome):100$");
	itemMeta_love.setDisplayName(ChatColor.YELLOW + "マジラブ(love):200$");

	List<String> lore_better = Arrays.asList(ChatColor.RESET + "効果:消滅の呪い", ChatColor.RESET + "相手には9$が入ります。");//説明文の作成
	List<String> lore_good = Arrays.asList(ChatColor.RESET + "効果:消滅の呪い", ChatColor.RESET + "相手には27$が入ります。");
	List<String> lore_like = Arrays.asList(ChatColor.RESET + "効果:消滅の呪い,ダメージ軽減1,耐久1", ChatColor.RESET + "相手には45$が入ります。");
	List<String> lore_awesome = Arrays.asList(ChatColor.RESET + "効果:消滅の呪い,ダメージ軽減2,耐久2,水中呼吸2", ChatColor.RESET + "相手には90$が入ります。");
	List<String> lore_love = Arrays.asList(ChatColor.RESET + "効果:消滅の呪い,束縛の呪い,ダメージ軽減3,耐久3,水中呼吸3,水中採掘3", ChatColor.RESET + "相手には180$が入ります。");

	itemMeta_better.setLore(lore_better);//説明文のセット
	itemMeta_good.setLore(lore_good);
	itemMeta_like.setLore(lore_like);
	itemMeta_awesome.setLore(lore_awesome);
	itemMeta_love.setLore(lore_love);

	choicegui_better.setItemMeta(itemMeta_better);//メタのセット
	choicegui_good.setItemMeta(itemMeta_good);
	choicegui_like.setItemMeta(itemMeta_like);
	choicegui_awesome.setItemMeta(itemMeta_awesome);
	choicegui_love.setItemMeta(itemMeta_love);

	headinv_choice.setItem(0,choicegui_better);
	headinv_choice.setItem(2,choicegui_good);
	headinv_choice.setItem(4,choicegui_like);
	headinv_choice.setItem(6,choicegui_awesome);
	headinv_choice.setItem(8,choicegui_love);


		player.openInventory(headinv_choice);

		player.getWorld().playSound(player.getLocation(),Sound.BLOCK_ANVIL_BREAK,1,1);

				}
				}
		}
	}

}
